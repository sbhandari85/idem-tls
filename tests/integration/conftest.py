import asyncio
import sys
import unittest.mock as mock
from typing import Any
from typing import Dict
from typing import List

import dict_tools
import pop.hub
import pytest


@pytest.fixture(scope="module", autouse=True)
def acct_profile() -> str:
    return "test_development_tls"


@pytest.fixture(scope="module", autouse=True, name="acct_subs")
def acct_subs() -> List[str]:
    return ["tls"]


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    for dyne in ["docs", "exec", "states", "tests"]:
        hub.pop.sub.add(dyne_name=dyne)

    # with mock.patch("sys.argv", ["idem_tls"]):
    #     hub.pop.config.load(
    #         ['acct', 'docs', 'exec', 'states', 'tests', 'tool'],
    #         cli="idem_tls"
    #     )

    yield hub


async def _setup_hub(hub):
    # Set up the hub before each function here
    ...


async def _teardown_hub(hub):
    # Clean up the hub after each function here
    ...


@pytest.fixture(scope="function", autouse=True)
async def function_hub_wrapper(hub):
    await _setup_hub(hub)
    yield
    await _teardown_hub(hub)


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    try:
        yield loop
    finally:
        loop.close()


@pytest.fixture(scope="module", name="hub")
def tpath_hub(code_dir, event_loop):
    """
    Add "idem_plugin" to the test path
    """
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        hub.pop.sub.add(dyne_name="idem")
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=False)
        hub.idem.RUNS = {"test": {}}

        yield hub


@pytest.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(
        run_name="test", test=False, tag="fake_|-test_|-tag"
    )

    # Add the profile to the account
    if hub.OPT.acct.acct_file and hub.OPT.acct.acct_key:
        await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
        ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)
    else:
        hub.log.warning("No credentials found, considering below config")
        ctx.acct = dict(
            method="TLSv1",
        )

    yield ctx
